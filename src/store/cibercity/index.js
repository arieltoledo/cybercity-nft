/* eslint-disable */

import axios from 'axios'
const OPENSEA_GALLERY_SLUG = process.env.OPENSEA_GALLERY_SLUG
const CYBERCITY_ROLES_CONTRACT_ADDRESS = process.env.CYBERCITY_ROLES_CONTRACT_ADDRESS

const TIER_2_SUPPLY = process.env.TIER_2_SUPPLY
const TIER_3_SUPPLY = process.env.TIER_3_SUPPLY

import {
  connectWallet,
  getBalance,
  getTokenData,
  claimRole,
  tokenUpgrade,
  stakeRole,
  getStakes,
  getStakeInfo,
  withdrawRewards,
  withdrawStake,
  getMintPack,
  mintPack,
  WithdrawStake
} from './interact.js'

const state = {
  walletAddress: '',
  walletStatus: '',
  walletConnected: false,
  walletWhitelisted: false,
  formatedWalletAddress: 'connect wallet',
  stage: '',
  tokenData: '',
  name: '',
  description: '',
  url: '',
  isLoading: false,
  transaction: '',
  walletBalance: '',
  walletTokens: [],
  walletTokensStatus: false,
  collectionTokens: [],
  walletStakes: [],
  walletStakesStatus: false,
  tokenStakeInfo: '',
  errorMessage: '',
  promotionTicketAmount: '',
  promotionTicketPrice: ''
}

const getters = {
  walletAddress: (state) => {
    return state.walletAddress
  },
  walletStatus: (state) => {
    return state.walletStatus
  },
  walletBalance: (state) => {
    return state.walletBalance
  },
  walletConnected: (state) => {
    return state.walletConnected
  },
  walletWhitelisted: (state) => {
    return state.walletWhitelisted
  },
  name: (state) => {
    return state.name
  },
  description: (state) => {
    return state.description
  },
  url: (state) => {
    return state.url
  },
  isLoading: (state) => {
    return state.isLoading
  },
  formatedWalletAddress: (state) => {
    return state.formatedWalletAddress
  },
  stage: (state) => {
    return state.stage
  },
  tokenData: (state) => {
    return state.tokenData
  },
  transaction: (state) => {
    return state.transaction
  },
  walletTokens: (state) => {
    return state.walletTokens
  },
  collectionTokens: (state) => {
    return state.collectionTokens
  },
  walletTokensStatus: (state) => {
    return state.walletTokensStatus
  },
  walletStakes: (state) => {
    return state.walletStakes
  },
  walletStakesStatus: (state) => {
    return state.walletStakesStatus
  },
  tokenStakeInfo: (state) => {
    return state.tokenStakeInfo
  },
  promotionTicketPrice: (state) => {
    return state.promotionTicketPrice
  },
  promotionTicketAmount: (state) => {
    return state.promotionTicketAmount
  }
}

const mutations = {
  setWallet (state, walletAddress) {
    state.walletAddress = walletAddress
  },
  setStatus (state, walletStatus) {
    state.walletStatus = walletStatus
  },
  setWalletBalance (state, walletBalance) {
    state.walletBalance = walletBalance
  },
  setWalletConnected (state, isConnected) {
    state.walletConnected = isConnected
  },
  setWalletWhitelisted (state, isWhitelisted) {
    state.walletWhitelisted = isWhitelisted
  },
  setUrl (state, url) {
    state.url = url
  },
  setName (state, name) {
    state.name = name
  },
  setDescription (state, description) {
    state.description = description
  },
  setFormatedWalletAddress (state) {
    const formated = state.walletAddress.toString().substring(0, 6) +
          '...' + state.walletAddress.toString().substring(38)
    state.formatedWalletAddress = state.walletStatus + ' ' + formated
  },
  setStage (state, stage) {
    state.stage = stage
  },
  setTokenData (state, tokenData) {
    state.tokenData = tokenData
  },
  setTransaction(state, transaction) {
    state.transaction = transaction
  },
  setLoading(state, loading) {
    state.isLoading = loading
  },
  setWalletTokens(state, walletTokens) {
    state.walletTokens = walletTokens
  },
  setWalletTokensStatus(state, status) {
    state.walletTokensStatus = status
  },
  setCollectionTokens (state, collectionToken) {
    state.collectionTokens.push(collectionToken)
  },
  setWalletStakes(state, walletStakes) {
      state.walletStakes = walletStakes
  },
  setWalletStakesStatus(state, status) {
    state.walletStakesStatus = status
  },
  setTokenStakeInfo(state, info) {
    state.tokenStakeInfo = info
  },
  setErrorMsg (state, error) {
    state.errorMessage = error
  },
  setPromotionTicketPrice(state, price) {
    state.promotionTicketPrice = price
  },
  setPromotionTicketAmount(state, amount) {
    state.promotionTicketAmount = amount
  }
}

const actions = {
  async loadInitialStage ({ commit }) {
  },
  async connectWallet ({ commit }) {
    commit('setLoading', true)
    const walletResponse = await connectWallet()
    getBalance()
    commit('setWallet', walletResponse.address)
    commit('setStatus', walletResponse.statusMsg)

    if (walletResponse.address != '') {
      commit('setWalletConnected', true)
      commit('setFormatedWalletAddress', walletResponse.address)
    }
    commit('setLoading', false)
  },
  async getWalletBalance( {commit} ) {
    commit('setLoading', true)
    const balanceResponse = await getBalance()
    commit('setWalletBalance', balanceResponse)
    commit('setLoading', false)
  },
  async getTokenData ( {commit }, {tokenId} ) {
    commit('setLoading', true)
    const response =  await getTokenData(tokenId)
    commit('setTokenData', response)
    commit('setLoading', false)
    return response
  },
  async claimRole ( {commit} ) {
    commit('setLoading', true)
    const response = await claimRole()
    commit('setTransaction', response)
    commit('setLoading', false)
  },
  async stakeRole ( {commit }, {tokenId} ) {
    commit('setLoading', true)
    const response = await stakeRole(tokenId)
    commit('setTransaction', response)
    commit('setLoading', false)
  },
  async mint ( {commit} ) {
    commit('setLoading', true)
    const mintResponse = await mint()
    commit('setTransaction', mintResponse)
    commit('setLoading', false)
  },
  async swapRedemption ( {commit} ) {
    commit('setLoading', true)
    const swapResponse = await swapRedemption()
    commit('setTransaction', swapResponse)
    commit('setLoading', false)
  },
  async getCollectionTokens ( {commit} ) {
    commit('setLoading', true)
      await axios.get('https://testnets-api.opensea.io/api/v1/assets?asset_contract_addresses=' + CYBERCITY_ROLES_CONTRACT_ADDRESS + '&order_direction=asc')//
      .then(response => {
        response.data.assets.forEach((asset) => {
          commit('setCollectionTokens', asset)
        })
        commit('setLoading', false)
      }).catch(error => {
        commit('setErrorMsg', error)
      })
  },
  async getWalletTokens ( {commit} ) {
    commit('setLoading', true)
    await axios.get('https://testnets-api.opensea.io/api/v1/assets?owner=' + state.walletAddress + '&order_direction=desc&offset=0')
    .then(response => {
      let assets = []
      response.data.assets.forEach((asset) => {
        if(asset.collection.slug === OPENSEA_GALLERY_SLUG) {
          if (parseInt(asset.token_id) > TIER_2_SUPPLY ) {
            asset.render_token_id = parseInt(asset.token_id) - TIER_2_SUPPLY
          }
          if (parseInt(asset.token_id) > TIER_3_SUPPLY) {
            asset.render_token_id = parseInt(asset.token_id) - TIER_3_SUPPLY
          }
          if(parseInt(asset.token_id) <= TIER_2_SUPPLY) {
              asset.render_token_id = parseInt(asset.token_id)
          }
          assets.push(asset)
        }
      })
      console.log(assets)
      commit('setWalletTokens', assets)
      commit('setWalletTokensStatus', true)
    }).catch(error => {
      commit('setErrorMsg', error)
    })
    commit('setLoading', false)
  },
  async getWalletStakes ( {commit} ) {
    commit('setLoading', true)
    commit('setWalletStakesStatus', false)
    const stakingTokens = await getStakes()
    let assets = []
    if (stakingTokens.length === 0) {
      assets = []
    } else {
      let url = 'https://testnets-api.opensea.io/api/v1/assets?'
      stakingTokens.forEach((token) => {
        url += 'token_ids=' + token + '&'
      })
      url += 'order_direction=desc&offset=0&collection=' + OPENSEA_GALLERY_SLUG + '&include_orders=false'
      await axios.get(url).then(response => {
        response.data.assets.forEach((asset) => {
          if(asset.collection.slug === OPENSEA_GALLERY_SLUG) {
            if (parseInt(asset.token_id) > TIER_2_SUPPLY ) {
              asset.render_token_id = parseInt(asset.token_id) - TIER_2_SUPPLY
            }
            if (parseInt(asset.token_id) > TIER_3_SUPPLY) {
              asset.render_token_id = parseInt(asset.token_id) - TIER_3_SUPPLY
            }
            if(parseInt(asset.token_id) <= TIER_2_SUPPLY) {
                asset.render_token_id = parseInt(asset.token_id)
            }
            assets.push(asset)
          }
        })
        console.log(assets)
        commit('setWalletStakes', assets)
        commit('setWalletStakesStatus', true)
      }).catch(error => {
        commit('setErrorMsg', error)
      })
    }
    commit('setLoading', false)
  },
  async getRewards({commit}, {tokenId}){
    commit('setLoading', true)
    const response = await withdrawRewards(tokenId)
    commit('setTransaction', response)
    commit('setLoading', false)
  },
  async withdrawStake({commit}, {tokenId}){
    commit('setLoading', true)
    const response = await withdrawStake(tokenId)
    commit('setTransaction', response)
    commit('setLoading', false)
  },
  async getTokenStakeInfo({ commit }, { tokenId }) {
    commit('setLoading', true)
    const stakeInfo = await getStakeInfo(tokenId)

    commit('setTokenStakeInfo', stakeInfo)
    commit('setLoading', false)
  },
  async tokenUpgrade( { commit }, {tokenId, promotionTickets } ) {
    commit('setLoading', true)
    const response = await tokenUpgrade(tokenId, promotionTickets)
    commit('setTransaction', response)
    commit('setLoading', false)
  },
  async getMintPack( {commit}, {amount} )  {
    commit('setLoading', true)
    const response = await getMintPack(amount)
    commit('setLoading', false)
    return response
  },
  async mintPack( {commit}, {amount, price} )  {
    commit('setLoading', true)
    const response = await mintPack(amount, price)
    commit('setTransaction', response)
    commit('setPromotionTicketAmount', amount)
    commit('setLoading', false)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
