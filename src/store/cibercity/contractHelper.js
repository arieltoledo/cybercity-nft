import Web3 from 'web3'

import cybercity from './contracts/cybercity/cybercity.json'
import cybercityroles from './contracts/cybercity-roles/cybercityroles.json'
import omnium from './contracts/omnium-token/omnium.json'
import CybercityRolePromotion from './contracts/roles-promotion/roles-promotion.json'

const RPCURL = process.env.RPCURL

const CYBERCITY_ABI = cybercity.abi
const CYBERCITY_ROLES_ABI = cybercityroles.abi
const OMNIUM_TOKEN_ABI = omnium.abi
const CYBERCITY_ROLES_PROMOTION_ABI = CybercityRolePromotion.abi

const ETHERSCAN_URL = process.env.ETHERSCAN_URL

const web3 = new Web3(RPCURL)

const contractGetter = {
  CYBERCITY_CONTRACT_ADDRESS: process.env.CYBERCITY_CONTRACT_ADDRESS,
  CYBERCITY_ROLES_CONTRACT_ADDRESS: process.env.CYBERCITY_ROLES_CONTRACT_ADDRESS,
  OMNIUM_TOKEN_CONTRACT_ADDRESS: process.env.OMNIUM_TOKEN_CONTRACT_ADDRESS,
  CYBERCITY_ROLES_PROMOTION_CONTRACT_ADDRESS: process.env.CYBERCITY_ROLES_PROMOTION
}

const contractLoader = {
  load (abi, contract) {
    window.contract = new web3.eth.Contract(abi, contract)
  },
  loadCyberCityContract () {
    this.load(CYBERCITY_ABI, contractGetter.CYBERCITY_CONTRACT_ADDRESS)
  },
  loadOmniumTokenContract () {
    this.load(OMNIUM_TOKEN_ABI, contractGetter.OMNIUM_TOKEN_CONTRACT_ADDRESS)
  },
  loadCyberCityRolesContract () {
    this.load(CYBERCITY_ROLES_ABI, contractGetter.CYBERCITY_ROLES_CONTRACT_ADDRESS)
  },
  loadRolesPromotionContract () {
    this.load(CYBERCITY_ROLES_PROMOTION_ABI, contractGetter.CYBERCITY_ROLES_PROMOTION_CONTRACT_ADDRESS)
  }
}

const transaction = {
  parameters: {
    to: '',
    from: '',
    value: '',
    data: ''
  },
  set paramaters (paramaters) {
    this.parameters = paramaters
  },
  get paramaters () {
    return this.parameters
  },
  async send () {
    try {
      const txHash = await window.ethereum
        .request({
          method: 'eth_sendTransaction',
          params: [this.parameters]
        })
      return {
        success: true,
        status: '✅ Check out your transaction on Etherscan:' + ETHERSCAN_URL + txHash,
        url: ETHERSCAN_URL + txHash
      }
    } catch (error) {
      return {
        success: false,
        status: '😥 Something went wrong: ' + error.message
      }
    }
  }
}

export default {
  contractLoader,
  contractGetter,
  transaction,
  web3
}
