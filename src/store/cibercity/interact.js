/* eslint-disable */
import contractHelper from './contractHelper'
import {date} from 'quasar'

const WINDOW = Object.create(contractHelper.contractLoader)
const CONTRACTS = Object.create(contractHelper.contractGetter)
const TRANSACTION = Object.create(contractHelper.transaction)
const web3 = contractHelper.web3

let walletAddress = ''

const connectWallet = async () => {
  if (window.ethereum) {
    try {
      const addressArray = await window.ethereum.request({
        method: 'eth_requestAccounts'
      })
      let obj = ''

      walletAddress = addressArray[0]

      obj = {
        statusMsg: 'Wallet connected',
        address: addressArray[0],
        connected: true,
      }

      return obj

    } catch (err) {
      return {
        address: '',
        status: '😥 ' + err.message,
        connected: false
      }
    }
  } else {
    return {
      address: '',
      status: '🦊  You must install Metamask, a virtual Ethereum wallet, in your browser.',
      connected: false
    }
  }
}

const getBalance = async () => {

  let balance = {
    redemption_tokens: '',
    resident_tokens: '',
    omnium_tokens: '',
    promotion_tickets: ''
  }

  // Get Residents Balance
  WINDOW.loadCyberCityContract()
  balance.resident_tokens = await window.contract.methods.balanceOf(walletAddress, 2).call()

  //Get OT balance
  WINDOW.loadOmniumTokenContract()
  balance.omnium_tokens = web3.utils.fromWei(await window.contract.methods.balanceOf(walletAddress).call())

  //Get Promotion Tickets Balance
  WINDOW.loadRolesPromotionContract()
  balance.promotion_tickets = await window.contract.methods.balanceOf(walletAddress, 1).call()

  return balance

}

const getTokenData = async (tokenId) => {

  WINDOW.loadCyberCityRolesContract()

  const tokenData = await window.contract.methods.getTokenData(tokenId).call()

  const tokenDataRes = {
    maxTokenSupply: tokenData[0],
    currentTokenSupply: tokenData[1],
    upgradePrice: tokenData[2],
    otkCoeficient: web3.utils.fromWei(tokenData[3])
  }

  return tokenDataRes
}

/******* STAKING ********/

const getStakes = async () => {

  WINDOW.loadOmniumTokenContract()

  const walletStakes = await window.contract.methods.getStakes(walletAddress, CONTRACTS.CYBERCITY_ROLES_CONTRACT_ADDRESS).call()

  return walletStakes

}

const getStakeInfo = async (tokenId) => {

  WINDOW.loadOmniumTokenContract()

  const stakeInfo = await window.contract.methods.getStakeInfo(walletAddress, CONTRACTS.CYBERCITY_ROLES_CONTRACT_ADDRESS, tokenId ).call()

  const fDate = new Date(new Date(Number(stakeInfo[1]) * 1000))

  const fStakeInfo = {
    amount: stakeInfo[0],
    date: date.formatDate(fDate, 'YYYY-MM-DD - HH:mm'),
    earnings: web3.utils.fromWei(stakeInfo[2])
  }

  return fStakeInfo

}
const stakeRole = async (tokenId) =>  {

  WINDOW.loadCyberCityRolesContract()

  TRANSACTION.paramaters = {
    to: CONTRACTS.CYBERCITY_ROLES_CONTRACT_ADDRESS,
    from: window.ethereum.selectedAddress,
    value: web3.utils.toHex(0),
    data: window.contract.methods.stakeRole(parseInt(tokenId)).encodeABI()
  }

  return TRANSACTION.send()

}

const withdrawRewards = async (tokenId) => {

  WINDOW.loadOmniumTokenContract()

  TRANSACTION.paramaters = {
    to: CONTRACTS.OMNIUM_TOKEN_CONTRACT_ADDRESS,
    from: window.ethereum.selectedAddress,
    value: web3.utils.toHex(0),
    data: window.contract.methods.withdrawRewards(CONTRACTS.CYBERCITY_ROLES_CONTRACT_ADDRESS, tokenId).encodeABI()
  }

  return TRANSACTION.send()
}

const withdrawStake = async (tokenId) => {

  WINDOW.loadOmniumTokenContract()

  TRANSACTION.paramaters = {
    to: CONTRACTS.OMNIUM_TOKEN_CONTRACT_ADDRESS,
    from: window.ethereum.selectedAddress,
    value: web3.utils.toHex(0),
    data: window.contract.methods.withdrawStake(CONTRACTS.CYBERCITY_ROLES_CONTRACT_ADDRESS, tokenId).encodeABI()
  }

  return TRANSACTION.send()

}

/******* ROLES ********/

const claimRole = async () => {

  WINDOW.loadCyberCityRolesContract()

  TRANSACTION.paramaters = {
    to: CONTRACTS.CYBERCITY_ROLES_CONTRACT_ADDRESS,
    from: window.ethereum.selectedAddress,
    value: web3.utils.toHex(0),
    data: window.contract.methods.claimRole().encodeABI()
  }

  return TRANSACTION.send()

}

/******* UPGRADE ********/

const getMintPack = async (amount) => {

  WINDOW.loadRolesPromotionContract()

  const results = await window.contract.methods.getMintPack(amount).call()

  return web3.utils.fromWei(results)

}

const approve = async (amount) => {

  WINDOW.loadOmniumTokenContract()

  TRANSACTION.paramaters = {
    to: CONTRACTS.OMNIUM_TOKEN_CONTRACT_ADDRESS,
    from: window.ethereum.selectedAddress,
    value: web3.utils.toHex(0),
    data: window.contract.methods.approve(CONTRACTS.CYBERCITY_ROLES_PROMOTION_CONTRACT_ADDRESS, amount).encodeABI()
  }

  return TRANSACTION.send()

}

const mintPack = async(amount, price) => {

  price = web3.utils.toWei(price.toString())

  const result = await approve(price)

  await delay(20000)

  if (result.success === true) {

    WINDOW.loadRolesPromotionContract()

    TRANSACTION.paramaters = {
      to: CONTRACTS.CYBERCITY_ROLES_PROMOTION_CONTRACT_ADDRESS,
      from: window.ethereum.selectedAddress,
      value: web3.utils.toHex(0),
      data: window.contract.methods.mintPack(amount).encodeABI()
    }

    return TRANSACTION.send()

  }

}

const tokenUpgrade = async (tokenId, promotionTickets)  => {

  WINDOW.loadRolesPromotionContract()

  const tokenIdparam = web3.eth.abi.encodeParameter('uint256', tokenId)

  TRANSACTION.paramaters = {
    to: CONTRACTS.CYBERCITY_ROLES_PROMOTION_CONTRACT_ADDRESS,
    from: window.ethereum.selectedAddress,
    value: web3.utils.toHex(0),
    data: window.contract.methods.safeTransferFrom(window.ethereum.selectedAddress,
      CONTRACTS.CYBERCITY_ROLES_CONTRACT_ADDRESS,1,
      parseInt(promotionTickets),tokenIdparam).encodeABI()
  }

  return TRANSACTION.send()

}

const delay = ms => new Promise(res => setTimeout(res, ms));

export {
  connectWallet,
  getBalance,
  claimRole,
  tokenUpgrade,
  getTokenData,
  stakeRole,
  getStakeInfo,
  getStakes,
  withdrawRewards,
  withdrawStake,
  getMintPack,
  mintPack,
  walletAddress
}

