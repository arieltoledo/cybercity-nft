'use strict'

Object.defineProperty(exports, '__esModule', {
  value: true
})
exports.getBalance = exports.getStage = exports.connectWallet = void 0

const _cybercity = _interopRequireDefault(require('./contracts/cybercity/cybercity.json'))

const _web = _interopRequireDefault(require('web3'))

function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

/* eslint-disable */

/* https://testnets-api.opensea.io/api/v1/collection/cyber-city-roles */

/* https://testnets-api.opensea.io/api/v1/assets?asset_contract_addresses=0x8215b5a80cd248557ab768a843084cbfdc896dda&order_direction=desc&offset=0&limit=20 */
var rpcURL = process.env.RPCURL;
var CYBERCITY_CONTRACT_ADDRESS = process.env.CYBERCITY_CONTRACT_ADDRESS;
var CYBERCITY_ROLES_CONTRACT_ADDRESS = process.env.CYBERCITY_ROLES_CONTRACT_ADDRESS;
var OMNIUM_TOKEN_CONTRACT_ADDRESS = process.env.OMNIUM_TOKEN_CONTRACT_ADDRESS;
var abi = _cybercity["default"].abi;
var web3 = new _web["default"](rpcURL);
var walletAddress = '';

var getStage = function getStage() {
  var currentStage, stageMsg;
  return regeneratorRuntime.async(function getStage$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(new web3.eth.Contract(abi, contractAddress));

        case 2:
          window.contract = _context.sent;
          _context.next = 5;
          return regeneratorRuntime.awrap(window.contract.methods.getSellStage().call());

        case 5:
          currentStage = _context.sent;
          stageMsg = '';

          if (currentStage == 1) {
            stageMsg = {
              token: 'REDEMPTION TOKEN',
              saleStatus: 'WHITELIST SALE',
              desc: 'A unique token with a limited supply of 1000. This token serves as the first entry into The Cyber City. Those lucky members who get their hands on at least 1 of these tokens will be able to then swap it in exchange for 1 Cyber City Resident Pass NFT.'
            };
          } else if (currentStage == 2) {
            stageMsg = {
              token: 'REDEMPTION TOKEN',
              saleStatus: 'PUBLIC SALE',
              desc: 'A unique token with a limited supply of 1000. This token serves as the first entry into The Cyber City. Those lucky members who get their hands on at least 1 of these tokens will be able to then swap it in exchange for 1 Cyber City Resident Pass NFT.'
            };
          } else if (currentStage == 3) {
            stageMsg = {
              token: 'RESIDENT PASS',
              saleStatus: 'WHITELIST SALE',
              desc: 'This pass will serve as your passport into the gates of Cyber City in the sandbox metaverse. Only those who have a Resident pass will be granted access to the city and participate in all the DAO has to offer its residents.'
            };
          } else if (currentStage == 4) {
            stageMsg = {
              token: 'RESIDENT PASS',
              saleStatus: 'FREE MINT',
              desc: 'This pass will serve as your passport into the gates of Cyber City in the sandbox metaverse. Only those who have a Resident pass will be granted access to the city and participate in all the DAO has to offer its residents.'
            };
          } else if (currentStage == 5) {
            stageMsg = {
              token: 'RESIDENT PASS',
              saleStatus: 'PUBLIC SALE',
              desc: 'This pass will serve as your passport into the gates of Cyber City in the sandbox metaverse. Only those who have a Resident pass will be granted access to the city and participate in all the DAO has to offer its residents.'
            };
          } else if (currentStage == 9) {
            stageMsg = {
              token: 'MINTING STARTING SOON',
              saleStatus: 'WL SOON',
              desc: ''
            };
          }

          return _context.abrupt("return", {
            sellStage: stageMsg,
            currentStageId: currentStage
          });

        case 9:
        case "end":
          return _context.stop();
      }
    }
  });
};

exports.getStage = getStage;

var connectWallet = function connectWallet() {
  var addressArray, obj;
  return regeneratorRuntime.async(function connectWallet$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          if (!window.ethereum) {
            _context2.next = 16;
            break;
          }

          _context2.prev = 1;
          _context2.next = 4;
          return regeneratorRuntime.awrap(window.ethereum.request({
            method: 'eth_requestAccounts'
          }));

        case 4:
          addressArray = _context2.sent;
          obj = '';
          walletAddress = addressArray[0];
          obj = {
            statusMsg: 'Wallet connected',
            address: addressArray[0],
            connected: true
          };
          return _context2.abrupt("return", obj);

        case 11:
          _context2.prev = 11;
          _context2.t0 = _context2["catch"](1);
          return _context2.abrupt("return", {
            address: '',
            status: '😥 ' + _context2.t0.message,
            connected: false
          });

        case 14:
          _context2.next = 17;
          break;

        case 16:
          return _context2.abrupt("return", {
            address: '',
            status: '🦊  You must install Metamask, a virtual Ethereum wallet, in your browser.',
            connected: false
          });

        case 17:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[1, 11]]);
};

exports.connectWallet = connectWallet;

var getBalance = function getBalance() {
  var balance;
  return regeneratorRuntime.async(function getBalance$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap(new web3.eth.Contract(abi, CYBERCITY_CONTRACT_ADDRESS));

        case 2:
          window.contract = _context3.sent;
          _context3.next = 5;
          return regeneratorRuntime.awrap(window.contract.methods.balanceOf(walletAddress, 1).call());

        case 5:
          _context3.t0 = _context3.sent;
          _context3.next = 8;
          return regeneratorRuntime.awrap(window.contract.methods.balanceOf(walletAddress, 2).call());

        case 8:
          _context3.t1 = _context3.sent;
          balance = {
            redemption_tokens: _context3.t0,
            resident_tokens: _context3.t1
          };
          return _context3.abrupt("return", balance);

        case 11:
        case "end":
          return _context3.stop();
      }
    }
  });
};

exports.getBalance = getBalance;

var getCurrentWalletConnected = function getCurrentWalletConnected() {
  var addressArray, stage, wl;
  return regeneratorRuntime.async(function getCurrentWalletConnected$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          if (!window.ethereum) {
            _context4.next = 23;
            break;
          }

          _context4.prev = 1;
          _context4.next = 4;
          return regeneratorRuntime.awrap(window.ethereum.request({
            method: 'eth_accounts'
          }));

        case 4:
          addressArray = _context4.sent;

          if (!(addressArray.length > 0)) {
            _context4.next = 15;
            break;
          }

          _context4.next = 8;
          return regeneratorRuntime.awrap(getStage());

        case 8:
          stage = _context4.sent;
          _context4.next = 11;
          return regeneratorRuntime.awrap(isWhitelisted(addressArray[0]));

        case 11:
          wl = _context4.sent;
          return _context4.abrupt("return", {
            address: addressArray[0],
            status: wl !== '' ? '👆🏽 Wallet connected. Start minting dude !!!! we are on ' + stage.sellStage + 'and you are on ' + wl : '👆🏽 Wallet connected. Start minting dude !!!! we are on ' + stage.sellStage
          });

        case 15:
          return _context4.abrupt("return", {
            address: '',
            status: '🦊 Connect to Metamask using the top right button.'
          });

        case 16:
          _context4.next = 21;
          break;

        case 18:
          _context4.prev = 18;
          _context4.t0 = _context4["catch"](1);
          return _context4.abrupt("return", {
            address: '',
            status: '😥 ' + _context4.t0.message
          });

        case 21:
          _context4.next = 24;
          break;

        case 23:
          return _context4.abrupt("return", {
            address: '',
            status: '🦊  You must install Metamask, a virtual Ethereum wallet, in your browser.'
          });

        case 24:
        case "end":
          return _context4.stop();
      }
    }
  }, null, null, [[1, 18]]);
};
