
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Platform.vue') }
    ]
  },
  {
    path: '/wallet_tokens',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/WalletTokens.vue') }
    ]
  },
  {
    path: '/ui',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Ui.vue') }
    ]
  },
  {
    path: '/platform',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Platform.vue') }
    ]
  },
  {
    path: '/roles',
    meta: { group: 'roles' },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Roles.vue') }
    ]
  },
  {
    path: '/upgrade',
    meta: { group: 'upgrade' },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Upgrade.vue') }
    ]
  },
  {
    path: '/stake',
    meta: { group: 'stake' },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Stake.vue') }
    ]
  },
  {
    path: '/role/token_id/:id',
    meta: { group: 'roles' },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Role.vue') }
    ]
  },
  {
    path: '/role_stake/token_id/:id',
    component: () => import('layouts/MainLayout.vue'),
    meta: { group: 'stake' },
    children: [
      { path: '', component: () => import('pages/RoleStake.vue') }
    ]
  },
  {
    path: '/upgrade/token_id/:id',
    meta: { group: 'upgrade' },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/UpgradeToken.vue') }
    ]
  },
  {
    path: '/upgrade/promotion_tickets/',
    meta: { group: 'upgrade' },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/PromotionStore.vue') }
    ]
  },
  {
    path: '/upgrade/promotion_tickets/:pack',
    meta: { group: 'upgrade' },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/PromotionTicketBought.vue') }
    ]
  },
  {
    path: '/role-claimed',
    meta: { group: 'roles' },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/RolClaimed.vue') }
    ]
  },
  {
    path: '/role-upgraded',
    meta: { group: 'roles' },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/RolUpgraded.vue') }
    ]
  },
  {
    path: '/test',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
