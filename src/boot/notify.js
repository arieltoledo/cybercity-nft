import { Notify } from 'quasar'

Notify.registerType('cc-positive', {
  icon: 'check',
  progress: false,
  classes: 'cc-notify-positive'
})

Notify.registerType('cc-negative', {
  icon: 'close',
  progress: false,
  classes: 'cc-notify-negative'
})

Notify.registerType('cc-warning', {
  icon: 'warning',
  progress: false,
  classes: 'cc-notify-warning'
})

Notify.registerType('cc-information', {
  icon: 'info',
  progress: false,
  classes: 'cc-notify-information'
})
